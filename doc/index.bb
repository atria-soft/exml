== [center]EXML library[/center] ==
__________________________________________________

===What is EXML, and how can I use it?===
EXML, or Ewol XML file interface, is a simple xml reader and writer.

===What languages are supported?===
EXML is written in C++.


===Are there any licensing restrictions?===
EXML is [b]FREE software[/b]


==== License (APACHE-2.0) ====
Copyright exml Edouard DUPIN

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.